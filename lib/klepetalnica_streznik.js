var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var socketGledeNaVzdevekId = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};

var vulgarna= {};

var geslaGledeNaKanal = {};

exports.listen = function(streznik, predpomnilnik) {
   vulgarna = predpomnilnik.split('\n');
   io = socketio.listen(streznik);
   io.set('log level', 1);
   io.sockets.on('connection', function (socket) {
    izpisUporabnikov(socket);
     stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
     pridruzitevKanalu(socket, 'Skedenj');
    izpisUporabnikov(socket);
     obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
     obdelajPosljiZasebnoSporocilo(socket)
     obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
     obdelajPridruzitevKanalu(socket);
     obdelajKanalGeslo(socket)
     socket.on('kanali', function() {
       socket.emit('kanali', io.sockets.manager.rooms);
     });
   
     obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    
   });
 };

function obdelajOdjavoUporabnika(socket) {
  
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
  
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevkiGledeNaSocket[socket.id] = vzdevek;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

//zacetek

function preveriVulgarne (sporocilo){
  for ( var i=0; i< vulgarna.length; i++){
    var len = vulgarna[i].length;
    var beseda1 = " "+vulgarna[i];
    var beseda2 = vulgarna[i]+" ";
    console.log("1: "+beseda1);
    console.log("2: "+beseda2);
    var re1 = new RegExp(beseda1, 'g');
    var re2 = new RegExp(beseda2, 'g');
    
    sporocilo = sporocilo.replace(re1, " "+rep(len));
    sporocilo = sporocilo.replace(re2, rep(len)+" ");
    console.log(("Sporocilo: "+sporocilo));
  }
  return sporocilo.toString();
}

function rep ( len ){
  var str="";
  for (var i=0; i < len; i++){
    str = str+ '*';
  }
  return str;
}

function izpisUporabnikov(socket){
  socket.emit('brisanjeUporabnikov', {user: "UrKo"});
  
  var uporabnikiNaKanalu = io.sockets.clients(trenutniKanal[socket.id]);
  
  var uporabnikSocketId;
  
  if (uporabnikiNaKanalu.length > 1) {
    for (var i in uporabnikiNaKanalu) {
      uporabnikSocketId = uporabnikiNaKanalu[i].id;
      
       socket.emit('dodajanjeUporabnikov', {user: vzdevkiGledeNaSocket[uporabnikSocketId]});
     
    }
   //console.log("UpoabnikiSocketId:"+ uporabnikSocketId);
   //console.log("VzdevkiGledeNaSocket:"+ vzdevkiGledeNaSocket);
   //console.log("Socket.id:"+ socket.id);
   for (var i in uporabnikiNaKanalu) {
      //console.log("vzdevki:"+ vzdevkiGledeNaSocket[i].toString());
      }
   
   //console.log("uporabljeniVzdevki:"+ uporabljeniVzdevki);
   
  }
}

function izpisUporabnika(socket, kanal){
  socket.emit('brisanjeUporabnikov', {user: "UrKo"});
  
  trenutniKanal[socket.id] = kanal;
  var uporabnikiNaKanalu = io.sockets.clients(trenutniKanal[socket.id]);
  var uporabnikSocketId;
  
  if (uporabnikiNaKanalu.length > 1) {
    for (var i in uporabnikiNaKanalu) {
      uporabnikSocketId = uporabnikiNaKanalu[i].id;
      
       socket.emit('dodajanjeUporabnikov', {user: vzdevkiGledeNaSocket[uporabnikSocketId]});
    }
  
  }
}


//konec

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: vzdevkiGledeNaSocket[socket.id]+" @ " + kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  socketGledeNaVzdevekId[vzdevkiGledeNaSocket[socket.id]] = socket.id;

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  izpisUporabnika(socket, kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
        //socket.emit('dodajanjeUporabnikov', {user: vzdevkiGledeNaSocket[uporabnikSocketId]});

      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        socketGledeNaVzdevekId[vzdevek] = socket.id;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
        socket.emit('pridruzitevOdgovor', {kanal: vzdevkiGledeNaSocket[socket.id]+" @ " + trenutniKanal[socket.id]});
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
 
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, kanal.novKanal);
  });
}

function obdelajPosredovanjeSporocila(socket) {
  
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + preveriVulgarne(sporocilo.besedilo)
      
    });
  });
  
}



//zacetek
function obdelajPosljiZasebnoSporocilo(socket) {
  socket.on('posljiZasebnoSMS', function(sporocilo) {
    
    if ( sporocilo.prejemnik == vzdevkiGledeNaSocket[socket.id]){
      socket.to(sporocilo.prejemnik).emit('sporocilo', {
            besedilo: "Sporočila \""+ sporocilo.besedilo +"\" uporabniku z vzdevkom \""+ sporocilo.prejemnik +"\" ni bilo mogoče posredovati."
          });
    }
    else{
    var poslano=false;
      for ( var i=0; i < uporabljeniVzdevki.length; i++){
        if ( sporocilo.prejemnik == uporabljeniVzdevki[i]){
          poslano = true;
          socket.to(sporocilo.prejemnik).emit('sporocilo', {
            besedilo: sporocilo.besedilo
          });
          trenutniKanal[socket.id]= 
           io.sockets.socket(socketGledeNaVzdevekId[sporocilo.prejemnik]).emit('sporocilo', {
            besedilo: vzdevkiGledeNaSocket[socket.id] + '(zasebno): ' + sporocilo.besedilo
          });
        }
      }
      if ( !poslano ){
        socket.to(sporocilo.prejemnik).emit('sporocilo', {
            besedilo: "Sporočila \""+ sporocilo.besedilo +"\" uporabniku z vzdevkom \""+ sporocilo.prejemnik +"\" ni bilo mogoče posredovati."
          });
      }
    }
    

  });
  
}
//konec

//zacetek
function obdelajKanalGeslo(socket) {
  socket.on('kanalPasswd', function(sporocilo) {
    //console.log("kanal: "+sporocilo.kanal+" geslo: "+sporocilo.geslo);
    if ( geslaGledeNaKanal[sporocilo.kanal] == null ){
      geslaGledeNaKanal[sporocilo.kanal] = sporocilo.geslo;
    }
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanaluGeslo(socket, sporocilo);
    
  });
}

function pridruzitevKanaluGeslo(socket, sporocilo) {
  //console.log("kanal: "+sporocilo.kanal+" geslo: "+sporocilo.geslo);
  if ( sporocilo.geslo == geslaGledeNaKanal[sporocilo.kanal]){
    socket.join(sporocilo.kanal);
    trenutniKanal[socket.id] = sporocilo.kanal;
    socket.emit('pridruzitevOdgovor', {kanal: sporocilo.kanal});
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + sporocilo.kanal + '.'
    });
  
    var uporabnikiNaKanalu = io.sockets.clients(sporocilo.kanal);
    if (uporabnikiNaKanalu.length > 1) {
      var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + sporocilo.kanal + ': ';
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        if (uporabnikSocketId != socket.id) {
          if (i > 0) {
            uporabnikiNaKanaluPovzetek += ', ';
          }
          uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
        }
      }
      uporabnikiNaKanaluPovzetek += '.';
      socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
    }
  }
  else{
    //console.log("Prijava ni uspela");
    socket.emit('sporocilo', {besedilo: "Pridruzitev v kanal "+sporocilo.kanal+" ni bilo uspesno, ker je geslo napacno!"});
  }
}
//konec



