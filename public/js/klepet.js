
var Klepet = function(socket) {
  this.socket = socket;
};


Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo 
  };
  this.socket.emit('sporocilo', sporocilo);
};

//zacetek
Klepet.prototype.posljiZasebnoSporocilo= function(kanal, prejemnik, sms) {
  var sporocilo = {
    kanal: "Skedenj",
    prejemnik: prejemnik,
    besedilo: sms
  };
  
  this.socket.emit('posljiZasebnoSMS', sporocilo);
};
//konec

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

//zacetek
Klepet.prototype.kanalGeslo = function(kanal, geslo) {
  var sporocilo ={
    kanal: kanal,
    geslo: geslo
  };
  console.log("kanalGeslo");
  this.socket.emit('kanalPasswd', sporocilo);
};
//konec

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();

  var sporocilo = "";


  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede[0].substring(0, besede[0].length);
      var str = besede.join(' ');
      besede.shift();
      var geslo = besede.join(' ');
      if ( kanal.charAt(0) == '"' ){
        kanal = kanal.substring(1, kanal.length-1);
        geslo = geslo.substring(1, geslo.length-1);
        this.kanalGeslo(kanal, geslo);
      }
      else{
        this.spremeniKanal(str);
      }
      break;
      
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
      
    case 'zasebno':
      besede.shift();
      var prejemnik =besede[0].substring(1, besede[0].length-1);
      besede.shift();
      var sms = besede.join(' ');
      sms = sms.substring(1, sms.length-1);
      
      console.log("Prejemnik: "+ prejemnik);
      console.log("sms: "+sms);
      
      //this.socket.emit('posljiZasebno', prejemnik, sms);
      this.posljiZasebnoSporocilo(kanal, prejemnik, sms);
      break;
      
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};