var http = require('http');
var path = require('path');
var mime = require('mime');
var fs  = require('fs');

var predpomnilnik = fs.readFileSync('/home/ubuntu/workspace/public/swearWords.txt', 'utf8');


function preveriVulgarne (besedilo){
  var str = besedilo;
  var vrstica = "";
  
  for (var i in predpomnilnik){
    vrstica = predpomnilnik[i];
    if ( vrstica == besedilo ){
      console.log(besedilo);
    }
  }
  return besedilo;
}


function posredujNapako404(odgovor) {
  odgovor.writeHead(404, {'Content-Type': 'text/plain'});
  odgovor.write('Napaka 404: Vira ni mogoče najti.');
  odgovor.end();
}

function posredujDatoteko(odgovor, datotekaPot, datotekaVsebina) {
  odgovor.writeHead(200, {"content-type-": mime.lookup(path.basename(datotekaPot))});
  odgovor.end(datotekaVsebina);
            //console.log( "pot: "+ datotekaPot );
            //console.log( "DatotekaVsebina: "+ datotekaVsebina );
            //console.log( "Odgovor: "+ odgovor.end(datotekaVsebina) );
}

function posredujStaticnoVsebino(odgovor, predpomnilnik, absolutnaPotDoDatoteke) {
  if (predpomnilnik[absolutnaPotDoDatoteke]) {
    posredujDatoteko(odgovor, absolutnaPotDoDatoteke, predpomnilnik[absolutnaPotDoDatoteke]);
  } else {
    fs.exists(absolutnaPotDoDatoteke, function(datotekaObstaja) {
      if (datotekaObstaja) {
        fs.readFile(absolutnaPotDoDatoteke, function(napaka, datotekaVsebina) {
          if (napaka) {
            posredujNapako404(odgovor);
          } else {
            
            predpomnilnik[absolutnaPotDoDatoteke] = datotekaVsebina;
            posredujDatoteko(odgovor, absolutnaPotDoDatoteke, datotekaVsebina);
            
          }
        });
      } else {
        posredujNapako404(odgovor);
      }
    });
  }
}

var streznik = http.createServer(function(zahteva, odgovor) {
  var potDoDatoteke = false;

  if (zahteva.url == '/') {
    potDoDatoteke = 'public/index.html';
  } else {
    potDoDatoteke = 'public' + zahteva.url;
  }

  var absolutnaPotDoDatoteke = './' + potDoDatoteke;
  posredujStaticnoVsebino(odgovor, predpomnilnik, absolutnaPotDoDatoteke);
  
});

streznik.listen(process.env.PORT, function() {
  console.log("Strežnik posluša na portu " + process.env.PORT + ".");
});

var klepetalnicaStreznik = require('./lib/klepetalnica_streznik');
klepetalnicaStreznik.listen(streznik, predpomnilnik);